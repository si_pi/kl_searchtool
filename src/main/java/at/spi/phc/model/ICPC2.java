/*
 * Copyright (c) 2023 Mag. DI Simon Pirker
 *
 * This program is free software: you can redistribute it and/or modify it under the terms of the GNU General Public License as published by the Free Software Foundation, either version 3 of the License, or (at your option) any later version.
 * This program is distributed in the hope that it will be useful, but WITHOUT ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the GNU General Public License for more details.
 * You should have received a copy of the GNU General Public License along with this program. If not, see https://www.gnu.org/licenses/.
 */
package at.spi.phc.model;

import lombok.*;
import java.util.List;

@Getter
@Setter
@AllArgsConstructor
@NoArgsConstructor
@Builder

public class ICPC2 {

    public String id;

    public String icpc2;

    public String begriffe;

    public String kriterien;

    public String redFlags;

    public String differentialdiagnosen;

    public String hinweise;

    public Integer komponente;

    public Integer farbe;

    public String snomedCT;

    public String snomedCTInternationalBrowser;

    public String substanzcode;

    public String icpc3;

    public String icd10;

    public String icd10Display;

    public String icd11;

    public String icd11Begriffe;
}
