FROM openjdk:17-jdk-alpine
ARG JAR_FILE=target/*.jar
COPY ${JAR_FILE} phc-0.1.3-RC3.jar
ENTRYPOINT ["java","-jar","/phc-1.0.0.jar"]